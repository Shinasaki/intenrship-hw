import os
import json
import random
import re
from pprint import pprint
import os

# ─── READ ───────────────────────────────────────────────────────────────────────

def checkChar(string):
  charRe = re.compile(r'[^a-zA-Z.]+')
  string = charRe.search(string)
  return not bool(string)

def read():

  """ Function for read category from json file """

  files = []

  # init category directory
  dirName = os.getcwd() + "\category"

  # get data and return
  for filename in os.listdir(dirName):
    with open(dirName + "\\" + filename) as file:
      files.append(json.load(file))

  return files

# ─── MESSAGE ────────────────────────────────────────────────────────────────────

def msg(msg, type=0):
  
  """ Function for response message to user
    :param msg: message.
    :param type: type of message to response [0 || empty, 1, 3]
  """

  if type == 1 : print(msg)
  else: 
    switcher = {
      "welcome" : "Welcome to the hangman game!!",
      "startUp" : "Please select category:",
    }
    print(switcher.get(msg))


# ─── GAME ───────────────────────────────────────────────────────────────────────
def game(data):

  """ Game func 
    : param data: data from category
  """
  restart = 0

  msg('startUp')

  # show category list
  for inx, category in enumerate(data):
    msg("[" + str(inx) + "] " + category["name"], 1)
  print("--------------------------")

  # get input from user
  select = input()
  if not select.isdigit():
    print("category must be `Number`.")
    print("--------------------------")
    main()
  elif int(select) >= len(data):
    print("the are only " + str(len(data)) + " type.")
    print("--------------------------")
    main()
  else:
    ct = data[int(select)]
    lg = len(ct['data']) - 1
    rd = round(random.random() * lg)

    print("Hint: " + str(ct['data'][rd]['hint']))
    gussing(ct['data'][rd])

# ─── GUSSING ────────────────────────────────────────────────────────────────────
def gussing(ct):
  score = 0
  remain = 5
  wrong = []
  guess = ""
  guesses = []
  word = ct['ans'].upper().lower()
  show = [""] * len(word)


  while(remain > 0):
    
    for idx, char in enumerate(word):

      # define current index

      if checkChar(char):

        if char in guesses:
          show[idx] = char
        
        else:
          show[idx] = "_ "

      else:
        show[idx] = char


    # show space
    print("".join(show))

    # input
    guess = input("GUESS 1 CHARECTOR: ")
    guesses.append(guess.lower())

    # condition check
    if guess.lower() not in "".join(re.findall("[a-zA-Z]+", word)):
      remain -= 1
      wrong.append(guess)

    else:
      score += 10

    print("\n\n--------------------------")
    print("SCORE: ", score)
    print("REMAIN GUESS: ", remain, "TIME")
    print("WRONG GUESSED: ", ", ".join(wrong))
    print("--------------------------")

    # Loose condition
    if (remain == 0):
      print("YOU LOOSE!")
  

    # Win condition
    if (score == len("".join(re.findall("[a-zA-Z]+", word)) * 10)):
      print("YOU WIN!")
      print("SCORE: ", score)
      print("REMAIN GUESS: ", remain, "time")
      print("WRONG: ", ", ".join(wrong))
      break

      
  

# ─── MAIN ───────────────────────────────────────────────────────────────────────

def main():
  msg('welcome')
  game(read())


#
# ──────────────────────────────────────────────── I ──────────
#   :::::: C A L L : :  :   :    :     :        :          :
# ──────────────────────────────────────────────────────────
#

main()